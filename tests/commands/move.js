'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { use, expect } from 'chai';
import * as mock from 'testdouble';
import asserttype from 'chai-asserttype';
import tdchai from 'testdouble-chai';

use(asserttype);
use(tdchai(mock));

describe('move handler', () => {
  let handler, data, victory, display, game, config;
  let input = {
    author: {
      author: 'author'
    },
    options: ['H7', 'H5']
  };

  beforeEach(async () => {
    let users = new Map(),
      games = new Map();

    game = {
      move: mock.function(),
      setPiece: mock.function(),
      removePiece: mock.function()
    };
    config = {
      previous_player: {},
      verbose: false,
      timeout: 0
    };

    users.set(input.author, game);
    games.set(game, config);

    data = {
      users,
      games
    };

    mock.replace('../../src/data.js', data);
    victory = mock.replace('../../src/victory.js').default;
    display = mock.replace('../../src/display.js').default;

    handler = (await import('../../src/commands/move.js')).default;
  });

  afterEach(() => {
    mock.reset();
    input.options = ['H7', 'H5'];
  });

  it('should be a function', () => {
    expect(handler).to.be.function();
  });

  it('should pass the first two options to the game move function', () => {
    handler(input);

    expect(game.move).to.have.been.calledWith(
      input.options[0],
      input.options[1]
    );
  });

  it('should return undefined on success', () => {
    mock.when(victory(input.author, false)).thenReturn('');

    expect(handler(input)).to.be.undefined;
  });

  it('should always call the victory handler', () => {
    handler(input);

    expect(victory).to.have.been.calledWith(
      input.author,
      mock.matchers.isA(Boolean)
    );
  });

  it('should return any strings retured by the victory handler', () => {
    mock.when(victory(input.author, false)).thenReturn('response');

    expect(handler(input)).to.match(/response/);
  });

  it('should set the previous player to the author', () => {
    handler(input);

    expect(config.previous_player).to.equal(input.author);
  });

  it('should return a representation of the board if verbose is set', () => {
    config.verbose = true;

    mock.when(display(game)).thenReturn('response');

    expect(handler(input)).to.match(/response/);
  });

  it("should not print the board if verbose isn't set", () => {
    handler(input);

    expect(display).not.to.have.been.called;
  });

  it('should send an error message if the timeout is exceeded', async () => {
    mock.when(victory(input.author, mock.matchers.isA(Boolean))).thenReturn('');

    config.timeout = 1;
    config.time = Date.now();

    await new Promise(r => setTimeout(r, 1000));

    expect(handler(input)).to.be.a('string');
  });

  it('should run normally if the timeout is not exceeded', async () => {
    mock.when(victory(input.author, false)).thenReturn('');

    config.timeout = 1;
    config.time = Date.now();

    await new Promise(r => setTimeout(r, 800));

    expect(handler(input)).to.be.undefined;
  });

  it('should ignore the timeout if the timeout value is zero', async () => {
    mock.when(victory(input.author, false)).thenReturn('');

    config.time = Date.now();

    await new Promise(r => setTimeout(r, 1000));

    expect(handler(input)).to.be.undefined;
  });

  it('should set the new timeout if the timeout is greater than zero', () => {
    config.timeout = 2;

    handler(input);

    expect(config.time).not.to.be.undefined;
  });

  it('should ignore the timeout if the last move time is undefined', async () => {
    mock.when(victory(input.author, false)).thenReturn('');

    config.timeout = 1;

    await new Promise(r => setTimeout(r, 1000));

    expect(handler(input)).to.be.undefined;
  });

  it('should ignore timeout if there is an error message', async () => {
    mock.when(victory(input.author, false)).thenReturn('');

    input.options.push('not valid');
    config.timeout = 1;
    config.time = Date.now();

    await new Promise(r => setTimeout(r, 1000));

    expect(handler(input)).not.to.match(/long/);
  });

  describe('playing twice', () => {
    beforeEach(() => {
      config.previous_player = input.author;
    });

    it('should return an error message if a player plays twice', () => {
      mock.when(victory(input.author, true)).thenReturn('');

      expect(handler(input)).to.be.a('string');
    });

    it('should not change the state if a player plays twice', () => {
      handler(input);

      expect(game.move).not.to.have.been.called;
    });
  });

  describe('illegal moves', () => {
    beforeEach(() => {
      mock
        .when(game.move(input.options[0], input.options[1]))
        .thenThrow(new Error());
    });

    it('should return an error message if an illegal move is made', () => {
      mock.when(victory(input.author, true)).thenReturn('');

      expect(handler(input)).to.be.a('string');
    });

    it('should call the victory handler with true if an illegal move is made', () => {
      handler(input);

      expect(victory).to.have.been.calledWith(input.author, true);
    });
  });

  describe('promotion', () => {
    beforeEach(() => {
      input.options.push('knight');
    });

    it('should remove the autoqueen if a promotion is done', () => {
      handler(input);

      expect(game.removePiece).to.have.be.calledWith(input.options[1]);
    });

    it('should place the specified piece if a promotion is done', () => {
      mock.when(game.removePiece(input.options[1])).thenReturn('q');

      handler(input);

      expect(game.setPiece).to.have.be.calledWith(
        input.options[1],
        input.options[2]
      );
    });

    it('should return an error message when promoting incorrectly', () => {
      mock.when(game.removePiece(input.options[1])).thenReturn('k');

      mock.when(victory(input.author, true)).thenReturn('');

      expect(handler(input)).to.be.a('string');
    });

    it('should call the victory handler with true when promoting incorrectly', () => {
      mock.when(game.removePiece(input.options[1])).thenReturn('k');

      handler(input);

      expect(victory).to.have.been.calledWith(input.author, true);
    });
  });

  describe('option verification', () => {
    it('should reject invalid options', () => {
      mock.when(victory(input.author, false)).thenReturn('');

      input.options.push('invalid');

      expect(handler(input)).to.be.a('string');
    });

    it('should have no side effects if verification fails', () => {
      input.options.push('invalid');

      handler(input);

      expect(game.move).not.to.have.been.called;
    });

    it('should call the victory handler with true if verification fails', () => {
      input.options.push('invalid');

      handler(input);

      expect(victory).to.have.been.calledWith(input.author, true);
    });

    describe('valid options', () => {
      let letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
        numbers = ['1', '2', '3', '4', '5', '6', '7', '8'],
        options = ['knight', 'bishop', 'rook', 'queen'];

      letters.forEach(letter =>
        numbers.forEach(number => options.push(letter + number))
      );

      options.forEach(option => {
        describe(option, () => {
          beforeEach(() => {
            mock
              .when(victory(input.author, mock.matchers.isA(Boolean)))
              .thenReturn('');
            input.options = [];
          });

          it(`should accept the ${option} option`, () => {
            input.options.push(option);

            expect(handler(input)).to.be.undefined;
          });

          it(`should reject strings with ${option} as a suffix`, () => {
            input.options.push('prefix' + option);

            expect(handler(input)).to.be.a('string');
          });

          it(`should reject strings with ${option} as a prefix`, () => {
            input.options.push(option + 'suffix');

            expect(handler(input)).to.be.a('string');
          });
        });
      });
    });
  });
});
