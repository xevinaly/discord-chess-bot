'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { use, expect } from 'chai';
import * as mock from 'testdouble';
import asserttype from 'chai-asserttype';
import tdchai from 'testdouble-chai';

use(asserttype);
use(tdchai(mock));

describe('start game handler', () => {
  let handler, data, display;
  let input = {
    author: {
      author: 'author'
    },
    users: [
      {
        userone: 'userone'
      },
      {
        usertwo: 'usertwo'
      }
    ],
    options: []
  };

  beforeEach(async () => {
    let users = new Map(),
      games = new Map();
    data = {
      users,
      games
    };

    mock.replace('../../src/data.js', data);
    display = mock.replace('../../src/display.js').default;

    handler = (await import('../../src/commands/start_game.js')).default;
  });

  afterEach(() => {
    mock.reset();
    input.options = [];
  });

  it('should be a function', () => {
    expect(handler).to.be.function();
  });

  it('should return undefined on sucess', () => {
    expect(handler(input)).to.be.undefined;
  });

  it('should add both users to the users map', () => {
    handler(input);

    expect(data.users).to.have.all.keys(input.users[0], input.users[1]);
  });

  it('should add a game to the games map', () => {
    handler(input);

    expect(data.games.size).to.be.above(0);
  });

  it('should associate the author with the added game', () => {
    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.author).to.equal(input.author);
  });

  it('should set the second user as the previous player', () => {
    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.previous_player).to.equal(input.users[1]);
  });

  it('should associate verbosity with the added game if indicated', () => {
    input.options.push('verbose');

    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.verbose).to.be.true;
  });

  it('should set verbosity to false otherwise', () => {
    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.verbose).to.be.false;
  });

  it('should return a representation of the board if verbose is set', () => {
    input.options.push('verbose');

    mock.when(display(mock.matchers.anything())).thenReturn('response');

    expect(handler(input)).to.match(/response/);
  });

  it("should not print the board if verbose isn't set", () => {
    handler(input);

    expect(display).not.to.have.been.called;
  });

  it('should associate a time limit with the added game if indicated', () => {
    input.options.push('10s');

    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.timeout).to.equal(10);
  });

  it('should set the time limit to 0 otherwise', () => {
    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.timeout).to.equal(0);
  });

  it('should set the movecount to 0', () => {
    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.moves).to.equal(0);
  });

  it('should set the piececount to 32', () => {
    handler(input);

    let config = data.games.get(data.users.get(input.users[0]));

    expect(config.pieces).to.equal(32);
  });

  describe('if a user is already in a game', () => {
    beforeEach(() => {
      input.users[0] = {
        toString: () => {
          return 'user';
        }
      };

      data.users.set(input.users[0], {});
    });

    afterEach(() => {
      input.users[0] = {
        userone: 'userone'
      };
    });

    it('should return an error message', () => {
      expect(handler(input)).to.be.a('string');
    });

    it('should collate with other error messages', () => {
      input.options.push('not valid');

      expect(handler(input)).to.equal(
        'Error: "not valid" is an invalid option,' +
          ' see usage with "chess help" for more information.\n' +
          'Error: user is already in a game,' +
          ' wait for it to end or restart the bot.\n'
      );
    });

    it('should have no side effects', () => {
      let users = new Map();
      let expected = {
        users: users,
        games: new Map()
      };

      users.set(input.users[0], {});

      handler(input);

      expect(data).to.deep.equal(expected);
    });
  });

  describe('option verification', () => {
    it('should reject invalid options', () => {
      input.options.push('invalid');

      expect(handler(input)).to.be.a('string');
    });

    it('should have no side effects if verification fails', () => {
      input.options.push('invalid');

      let expected = {
        users: new Map(),
        games: new Map()
      };

      handler(input);

      expect(data).to.deep.equal(expected);
    });

    describe('time limits', () => {
      it('should accept the time limit option', () => {
        input.options.push('1s');
        input.options.push('12s');
        input.options.push('100s');

        expect(handler(input)).to.be.undefined;
      });

      it('should reject time limits equal to zero', () => {
        input.options.push('0s');

        expect(handler(input)).to.be.a('string');
      });

      describe('prefixes', () => {
        it('should reject strings with 1s as a prefix', () => {
          input.options.push('prefix-1s');

          expect(handler(input)).to.be.a('string');
        });

        it('should reject strings with 10s as a prefix', () => {
          input.options.push('prefix-10s');

          expect(handler(input)).to.be.a('string');
        });
      });

      describe('suffixes', () => {
        it('should reject strings with 1s as a suffix', () => {
          input.options.push('1s-suffix');

          expect(handler(input)).to.be.a('string');
        });

        it('should reject strings with 10s as a suffix', () => {
          input.options.push('10s-suffix');

          expect(handler(input)).to.be.a('string');
        });
      });
    });

    describe('verbose', () => {
      it('should accept the verbose option', () => {
        input.options.push('verbose');

        expect(handler(input)).to.be.undefined;
      });

      it('should reject strings with verbose as a suffix', () => {
        input.options.push('prefix-verbose');

        expect(handler(input)).to.be.a('string');
      });

      it('should reject strings with verbose as a prefix', () => {
        input.options.push('verbose-suffix');

        expect(handler(input)).to.be.a('string');
      });
    });
  });
});
