'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { use, expect } from 'chai';
import * as mock from 'testdouble';
import asserttype from 'chai-asserttype';
import tdchai from 'testdouble-chai';

use(asserttype);
use(tdchai(mock));

describe('cleanup', () => {
  let cleanup, data, game;
  let userone = {
      userone: 'userone'
    },
    usertwo = {
      usertwo: 'usertwo'
    };

  beforeEach(async () => {
    let users = new Map(),
      games = new Map();

    game = {
      game: 'game'
    };

    users.set(userone, game);
    users.set({}, {});
    users.set(usertwo, game);
    games.set(game, {});

    data = {
      users,
      games
    };

    mock.replace('../src/data.js', data);

    cleanup = (await import('../src/cleanup.js')).default;
  });

  afterEach(() => {
    mock.reset();
  });

  it('should be a function', () => {
    expect(cleanup).to.be.function();
  });

  it('should remove the game from the games map', () => {
    cleanup(game);

    expect(data.games.has(game)).to.be.false;
  });

  it('should remove the players from the user map', () => {
    cleanup(game);

    expect(data.users).not.to.include(game);
  });

  it('should not remove other players', () => {
    cleanup(game);

    expect(data.users.size).to.equal(1);
  });
});
