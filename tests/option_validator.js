'use strict';

import { describe, it } from 'mocha';
import { use, expect } from 'chai';
import asserttype from 'chai-asserttype';

import validator from '../src/option_validator.js';

use(asserttype);

describe('option validator', () => {
  it('should be a function', () => {
    expect(validator).to.be.function();
  });

  it('it should return an empty string if all options are valid', () => {
    let recieved = ['valid', 'also valid', 'also valid'],
      expected = [/^valid$/, /^also valid$/];

    expect(validator(recieved, expected)).to.equal('');
  });

  it('it should return an error message if an option is invalid', () => {
    let recieved = ['valid', 'also valid', 'not valid'],
      expected = [/^valid$/, /^also valid$/];

    expect(validator(recieved, expected)).to.equal(
      'Error: "not valid" is an invalid option,' +
        ' see usage with "chess help" for more information.\n'
    );
  });

  it('it should chain error messages if multiple options are invalid', () => {
    let recieved = ['not valid', 'also not valid'],
      expected = [/^valid$/, /^also valid$/];

    expect(validator(recieved, expected)).to.equal(
      'Error: "not valid" is an invalid option,' +
        ' see usage with "chess help" for more information.\n' +
        'Error: "also not valid" is an invalid option,' +
        ' see usage with "chess help" for more information.\n'
    );
  });
});
