'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { use, expect } from 'chai';
import * as mock from 'testdouble';
import asserttype from 'chai-asserttype';
import tdchai from 'testdouble-chai';

use(asserttype);
use(tdchai(mock));

describe('message handler', () => {
  let parser, start_command, move_command, tourney_command, handler;
  let users = new Map();

  beforeEach(async () => {
    parser = mock.replace('../../src/command_parser.js').default;
    start_command = mock.replace('../../src/commands/start_game.js').default;
    move_command = mock.replace('../../src/commands/move.js').default;
    tourney_command = mock.replace('../../src/commands/start_tourney.js')
      .default;
    mock.replace('../../src/data.js', {
      users
    });

    handler = (await import('../../src/handlers/message.js')).default;
  });

  afterEach(() => {
    mock.reset();
  });

  it('should be a function', () => {
    expect(handler).to.be.function();
  });

  it('should reject null input', () => {
    expect(handler({})).to.be.false;
  });

  it('should reject empty content', () => {
    expect(handler({ content: '' })).to.be.false;
  });

  it('should reject unrecognised commands', () => {
    expect(handler({ content: 'End Game' })).to.be.false;
  });

  describe('help command', () => {
    let accepted = {
      content: 'chess help',
      channel: {
        send: mock.function()
      }
    };

    it('should accept the command regardless of arguments and mentions', () => {
      expect(handler(accepted)).to.be.true;
    });

    it('should send instructions', () => {
      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(
        mock.matchers.isA(String)
      );
    });
  });

  describe('rules command', () => {
    let accepted = {
      content: 'chess rules',
      channel: {
        send: mock.function()
      }
    };

    it('should accept the command regardless of arguments and mentions', () => {
      expect(handler(accepted)).to.be.true;
    });

    it('should send the rules', () => {
      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(
        mock.matchers.isA(String)
      );
    });
  });

  describe('start game command', () => {
    let content = 'Start Game <@!1> <@!2> verbose 10s';
    let response = '<@!1> is already in a game!';
    let accepted = {
      content: content,
      channel: {
        send: mock.function()
      }
    };

    beforeEach(() => {
      mock.when(start_command(mock.matchers.anything())).thenReturn(response);
    });

    it('should accept exactly two mentions', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should reject less than two mentions', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1'],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should reject more than two mentions', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2', 'U3'],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should accept zero arguments', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: []
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should accept one argument', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should accept two arguments', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should reject more than two arguments', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2', 'O3']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should send usage instructions when it fails verification', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2', 'O3']
      });

      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(
        mock.matchers.isA(String)
      );
    });

    it('should do nothing if the command handler returns undefined', () => {
      let response = {
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2']
      };
      mock.when(parser('start game', accepted)).thenReturn(response);
      mock.when(start_command(response)).thenReturn(undefined);

      handler(accepted);

      expect(accepted.channel.send).not.to.have.been.called;
    });

    it('should send strings returned from the command handler', () => {
      mock.when(parser('start game', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2']
      });

      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(response);
    });
  });

  describe('start tourney command', () => {
    let content = 'Start tourney <@!1> <@!2> verbose 10s';
    let response = '<@!1> is already in a game!';
    let accepted = {
      content: content,
      channel: {
        send: mock.function()
      }
    };

    beforeEach(() => {
      mock.when(tourney_command(mock.matchers.anything())).thenReturn(response);
    });

    it('should reject less than two mentions', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1'],
        options: []
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should accept two mentions', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should accept more than two mentions', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2', 'U3'],
        options: ['O1']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should accept zero arguments', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: []
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should accept one argument', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should reject more than one argument', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should send usage instructions when it fails verification', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1', 'O2']
      });

      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(
        mock.matchers.isA(String)
      );
    });

    it('should do nothing if the command handler returns undefined', () => {
      let response = {
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1']
      };
      mock.when(parser('start tourney', accepted)).thenReturn(response);
      mock.when(tourney_command(response)).thenReturn(undefined);

      handler(accepted);

      expect(accepted.channel.send).not.to.have.been.called;
    });

    it('should send strings returned from the command handler', () => {
      mock.when(parser('start tourney', accepted)).thenReturn({
        author: 'A',
        users: ['U1', 'U2'],
        options: ['O1']
      });

      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(response);
    });
  });

  describe('move command', () => {
    let content = 'CL NL';
    let response = '<@!1> has won!';
    let registered = {
      registered: 'registered'
    };
    let game = {
      game: 'game'
    };
    let accepted = {
      author: registered,
      content: content,
      channel: {
        send: mock.function()
      }
    };

    users.set(registered, game);

    beforeEach(() => {
      mock
        .when(move_command(mock.matchers.anything(), mock.matchers.anything()))
        .thenReturn(response);
    });

    it('should interpret messages from saved users', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should ignore messages from unsaved users', () => {
      let unregistered = {
        unregistered: 'unregistered'
      };
      let rejected = {
        author: unregistered,
        content: 'CL NL'
      };

      handler(rejected);

      expect(parser).not.to.have.been.called;
    });

    it('should accept exactly zero mentions', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should reject more than zero mentions', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: ['U1'],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should reject less than two arguments', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should accept two arguments', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1', 'O2']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should accept three argument', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1', 'O2', 'O3']
      });

      expect(handler(accepted)).to.be.true;
    });

    it('should reject more than three arguments', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1', 'O2', 'O3', 'O4']
      });

      expect(handler(accepted)).to.be.false;
    });

    it('should reject if the move_command handler returns undefined', () => {
      let response = {
        author: 'A',
        users: [],
        options: []
      };
      mock.when(parser('', accepted)).thenReturn(response);
      mock.when(move_command(response)).thenReturn(undefined);

      expect(handler(accepted)).to.be.false;
    });

    it('should send strings returned from the move_command handler', () => {
      mock.when(parser('', accepted)).thenReturn({
        author: 'A',
        users: [],
        options: ['O1', 'O2']
      });

      handler(accepted);

      expect(accepted.channel.send).to.have.been.calledWith(response);
    });

    describe('ignoring other commands', () => {
      beforeEach(() => {
        mock
          .when(parser(mock.matchers.anything(), mock.matchers.anything()))
          .thenReturn({
            author: 'A',
            users: [],
            options: ['O1', 'O2', 'O3']
          });
      });

      it('should ignore the help command', () => {
        handler({
          content: 'chess help',
          mentions: [],
          channel: {
            send: mock.function()
          }
        });

        expect(parser).not.to.have.been.calledWith('', accepted);
      });

      it('should ignore the rules command', () => {
        handler({
          content: 'chess rules',
          mentions: [],
          channel: {
            send: mock.function()
          }
        });

        expect(parser).not.to.have.been.calledWith('', accepted);
      });
      it('should ignore the start game command', () => {
        handler({
          content: 'start game',
          mentions: [],
          channel: {
            send: mock.function()
          }
        });

        expect(parser).not.to.have.been.calledWith('', accepted);
      });

      it('should ignore the start tourney command', () => {
        handler({
          content: 'start tourney',
          mentions: [],
          channel: {
            send: mock.function()
          }
        });

        expect(parser).not.to.have.been.calledWith('', accepted);
      });
    });
  });
});
