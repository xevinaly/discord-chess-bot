'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { use, expect } from 'chai';
import * as mock from 'testdouble';
import asserttype from 'chai-asserttype';
import tdchai from 'testdouble-chai';

use(asserttype);
use(tdchai(mock));

describe('victory handler', () => {
  let handler, cleanup, data, author, game, config, json;

  beforeEach(async () => {
    let users = new Map(),
      games = new Map();

    game = {
      exportJson: mock.function()
    };
    config = {
      author: {
        toString: mock.function()
      },
      moves: 0,
      pieces: 32
    };
    json = {
      pieces: {},
      isFinished: false,
      checkMate: false
    };
    author = {
      toString: mock.function()
    };

    users.set(author, game);
    games.set(game, config);

    data = {
      users,
      games
    };

    mock.when(game.exportJson()).thenReturn(json);
    mock.when(author.toString()).thenReturn('author');

    mock.replace('../src/data.js', data);
    cleanup = mock.replace('../src/cleanup.js').default;

    handler = (await import('../src/victory.js')).default;
  });

  afterEach(() => {
    mock.reset();
  });

  it('should be a function', () => {
    expect(handler).to.be.function();
  });

  it('should return an empty string when there is no victor', () => {
    expect(handler(author, false)).to.equal('');
  });

  it('should recognise a win by checkMate', () => {
    json.isFinished = true;
    json.checkMate = true;

    expect(handler(author, false)).to.match(/author/);
  });

  it('should recognise a forfeit', () => {
    expect(handler(author, true)).to.match(/author/);
  });

  it('should recognise a draw', () => {
    json.isFinished = true;

    expect(handler(author, false)).to.match(/draw/);
  });

  it('should recognise a draw by moves', () => {
    config.moves = 49;

    expect(handler(author, false)).to.match(/draw/);
  });

  it('should increment the move count when the pieces are unequal', () => {
    handler(author, false);

    expect(config.moves).to.equal(1);
  });

  it('should set the move count to zero when the pieces are equal', () => {
    config.pieces = 0;
    config.moves = 1;

    handler(author, false);

    expect(config.moves).to.equal(0);
  });

  it('should set the piece count', () => {
    handler(author, false);

    expect(config.pieces).to.equal(0);
  });

  it('should not call cleanup if the game will continue', () => {
    handler(author, false);

    expect(cleanup).not.to.have.been.called;
  });

  it('should call cleanup if the game ends', () => {
    handler(author, true);

    expect(cleanup).to.have.been.calledWith(game);
  });

  it('should mention the creator of the game', () => {
    handler(author, true);

    expect(config.author.toString).to.have.been.called;
  });
});
