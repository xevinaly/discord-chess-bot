'use strict';

import { describe, it } from 'mocha';
import { use, expect } from 'chai';
import asserttype from 'chai-asserttype';

use(asserttype);

import parser from '../src/command_parser.js';

describe('command parser', () => {
  let users = new Map();
  let user_one = {
    userone: 'userone'
  };
  let user_two = {
    usertwo: 'usertwo'
  };
  let author = {
    author: 'author'
  };
  let input = {
    content: 'Start Game <@!123456789> <@!234567890> verbose 10s',
    author,
    mentions: {
      users
    }
  };

  users.set('123456789', user_one);
  users.set('234567890', user_two);

  it('should be a function', () => {
    expect(parser).to.be.function();
  });

  it('should parse the author and return it', () => {
    expect(parser('', input).author).to.deep.equal(author);
  });

  it('should parse the mentions and return them', () => {
    expect(parser('', input).users)
      .to.be.an('array')
      .which.deep.equal([user_one, user_two]);
  });

  describe('option parsing', () => {
    it('should parse the options and return them', () => {
      let options = ['Start', 'Game', 'verbose', '10s'];

      expect(parser('', input).options)
        .to.be.an('array')
        .which.deep.equal(options);
    });

    it('should return an empty array if there are no options', () => {
      let empty = { ...input };
      empty.content = 'start game';

      expect(parser('start game', empty).options)
        .to.be.an('array')
        .which.deep.equal([]);
    });

    it('should ignore text mentions', () => {
      expect(parser('', input).options)
        .to.be.an('array')
        .that.does.not.include('<@!123456789>')
        .and.does.not.include('<@!234567890>');
    });

    it('should ignore command prefixes', () => {
      expect(parser('start game', input).options)
        .to.be.an('array')
        .that.does.not.include('Start')
        .and.does.not.include('Game');
    });
  });
});
