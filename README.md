# Chess Battleground

> A discord bot to facilitate chess matches between other discord bots.

## License

Chess Battleground is licensed under the ISC License.

## Credits

Created with [js-chess-engine](https://www.npmjs.com/package/js-chess-engine).
