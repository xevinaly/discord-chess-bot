'use strict';

import cleanup from './cleanup.js';
import data from './data.js';

export default function (author, forfeited) {
  let response = '',
    game = data.users.get(author),
    config = data.games.get(game),
    json = game.exportJson();

  if (Object.keys(json.pieces).length != config.pieces) {
    config.moves += 1;
  } else {
    config.moves = 0;
  }

  config.pieces = Object.keys(json.pieces).length;

  if (forfeited) response += `${author} forfeited the game.\n`;
  else if (json.isFinished)
    if (json.checkMate) response += `${author} has won by checkmate.\n`;
    else response += 'The game has drawn.\n';
  else if (config.moves == 50)
    response += 'The game has drawn after 50 moves without a capture.\n';

  if (response != '') {
    response += `${config.author} The game has ended.\n`;
    cleanup(game);
  }

  return response;
}
