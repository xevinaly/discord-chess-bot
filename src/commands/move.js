'use strict';

import validator from '../option_validator.js';
import victory from '../victory.js';
import display from '../display.js';
import data from '../data.js';

const expected = [/^knight$/, /^bishop$/, /^rook$/, /^queen$/, /^[A-H][1-8]$/];

export default function (command) {
  let response = validator(command.options, expected),
    game = data.users.get(command.author),
    config = data.games.get(game);

  if (command.author == config.previous_player)
    response += `Error: ${command.author} moved twice in a row\n`;

  config.previous_player = command.author;

  if (response == '') {
    try {
      game.move(command.options[0], command.options[1]);

      if (command.options.length == 3) {
        if (game.removePiece(command.options[1]).toLowerCase() != 'q')
          response += `Error: ${command.author} promoted incorrectly\n`;
        game.setPiece(command.options[1], command.options[2]);
      }
    } catch (ignore) {
      response += `Error: ${command.author} made an invalid move\n`;
    }
  }

  if (config.timeout > 0 && response == '') {
    if (Math.floor((Date.now() - config?.time) / 1000) >= config.timeout)
      response += `Error: ${command.author} took too long\n`;
    config.time = Date.now();
  }

  response += victory(command.author, response != '');

  if (config.verbose) response += display(game);

  if (response != '') return response;
}
