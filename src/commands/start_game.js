'use strict';

import * as engine from 'js-chess-engine';
import validator from '../option_validator.js';
import display from '../display.js';
import data from '../data.js';

const expected = [/^verbose$/, /^([1-9]+)s$|^1(0+)s$/];

export default function (command) {
  let response = validator(command.options, expected),
    game = new engine.Game(),
    config = {
      author: command.author,
      previous_player: command.users[1],
      moves: 0,
      pieces: 32,
      verbose: command.options.includes('verbose'),
      timeout: calculateTimeout(command.options)
    };

  command.users.forEach(user => {
    if (data.users.has(user))
      response +=
        `Error: ${user} is already in a game,` +
        ' wait for it to end or restart the bot.\n';
  });

  if (response != '') return response;

  command.users.forEach(user => data.users.set(user, game));

  data.games.set(game, config);

  if (command.options.includes('verbose')) return display(game);
}

function calculateTimeout(options) {
  let timeout = 0;

  options.forEach(option => {
    if (option.match(expected[1])) timeout = option.match(/\d+/)[0] - 0;
    expected[1].lastIndex = 0;
  });

  return timeout;
}
