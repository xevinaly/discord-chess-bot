'use strict';

export default function (recieved, expected) {
  let response = '';

  recieved.forEach(option => {
    let found = false;

    expected.forEach(valid => {
      found = found || option.match(valid);
      valid.lastIndex = 0;
    });

    if (!found)
      response +=
        `Error: "${option}" is an invalid option,` +
        ' see usage with "chess help" for more information.\n';
  });

  return response;
}
