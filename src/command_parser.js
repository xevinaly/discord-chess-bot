'use strict';

export default function (prefix, message) {
  let users = [];
  let content = message.content.trim().substring(prefix.length);

  let mention;

  do {
    mention = /<@!([0-9]+)>/g.exec(content);
    if (mention) {
      let id = /([0-9]+)/g.exec(mention[0]);
      users.push(message.mentions.users.get(id[0]));
      content = content.replace(mention[0], '');
    }
  } while (mention);

  content = content.trim().replace(/( +)/g, ' ');

  return {
    author: message.author,
    users,
    options: content == '' ? [] : content.split(' ')
  };
}
