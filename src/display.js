'use strict';

export default function (game) {
  return format(captureConsoleLog(game));
}

function captureConsoleLog(game) {
  var oldLog = process.stdout.write;
  var output = '';

  process.stdout.write = message => (output = output.concat(message));

  game.printToConsole();

  process.stdout.write = oldLog;

  return output;
}

function format(generated) {
  const divider = '╟─┼─┼─┼─┼─┼─┼─┼─╢┊\n';

  let output = '```';
  output += '╔═╤═╤═╤═╤═╤═╤═╤═╗╮┊\n';

  generated
    .replace(/[A-Z]/g, '')
    .trim()
    .split('\n')
    .forEach(line => {
      output += '║';

      line
        .replace(/[0-9]/g, '')
        .split('')
        .forEach((char, key, array) => {
          output += char;
          if (key != array.length - 1) output += '│';
        });

      output += '║' + line.charAt(0) + '\n';
      output += divider;
    });

  output += '╚═╧═╧═╧═╧═╧═╧═╧═╝┊\n';
  output += '╰a┈b┈c┈d┈e┈f┈g┈h┈╯';
  return (output += '```');
}
