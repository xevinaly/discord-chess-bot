'use strict';

import { config } from 'dotenv';
import { Client } from 'discord.js';

import handler from './handlers/message.js';

config();

const client = new Client();

client.on('message', handler);

client.login(process.env.CHESS_BOT_TOKEN);
