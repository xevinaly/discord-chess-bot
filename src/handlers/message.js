'use strict';

import parser from '../command_parser.js';
import start_game from '../commands/start_game.js';
import move from '../commands/move.js';
import start_tourney from '../commands/start_tourney.js';
import data from '../data.js';

const start_game_usage =
  '```Usage: start game @Bot1 @Bot2 [options]\n' +
  'Options:\n' +
  ' - verbose: print the board every turn\n' +
  ' - [x]s: not responding in x seconds results in an immediate loss. Must be greater than 0.```';
const start_tourney_usage =
  '```Usage: start tourney @Bot1 @Bot2 ... @BotN ?type\n' +
  'Types:\n' +
  ' - single: single elimination bracket\n' +
  ' - double: double elimination bracket```';
const move_usage =
  '```Usage: current_space next_space pawn_promotion_choice```';
const rules_usage = '```Usage: chess rules```';
const rules =
  '```Rules:\n' +
  '1) Making an invalid move results in a loss\n' +
  '2) When applicable, timing out results in a loss```';

export default function (message) {
  let response;

  if (message?.content === undefined) return false;

  if (message.content.toLowerCase().startsWith('chess help'))
    message.channel.send(
      '**Commands:**\n' +
        start_game_usage +
        '\n' +
        start_tourney_usage +
        '\n' +
        move_usage +
        '\n' +
        rules_usage
    );
  else if (message.content.toLowerCase().startsWith('chess rules'))
    message.channel.send(rules);
  else if (message.content.toLowerCase().startsWith('start game')) {
    let command = parser('start game', message);

    if (command.users.length != 2 || command.options.length > 2) {
      message.channel.send(start_game_usage);
      return false;
    }

    response = start_game(command);
  } else if (message.content.toLowerCase().startsWith('start tourney')) {
    let command = parser('start tourney', message);

    if (command.users.length < 2 || command.options.length > 1) {
      message.channel.send(start_tourney_usage);
      return false;
    }

    response = start_tourney(command);
  } else if (data.users.has(message.author)) {
    let command = parser('', message);

    if (
      command.users.length != 0 ||
      command.options.length < 2 ||
      command.options.length > 3
    )
      return false;

    response = move(command, data.users.get(message.author));
  } else return false;

  if (response != undefined) message.channel.send(response);

  return true;
}
