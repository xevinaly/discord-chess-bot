'use strict';

import data from './data.js';

export default function (game) {
  for (let entry of data.users)
    if (entry[1] == game) data.users.delete(entry[0]);

  data.games.delete(game);
}
